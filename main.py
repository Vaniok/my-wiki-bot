# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import jsonify
import requests
import json
import random
from config import TOKEN



app = Flask(__name__)



URL = 'https://api.telegram.org/' + TOKEN


@app.route('/', methods=['POST', 'GET'])
def command():

    if request.method == 'POST':
        event = request.get_json()
        #write_json(event)

        if 'edited_message' in event:
            send_message(event['edited_message']['chat']['id'], f'не редагуй повідомлення!')

        elif 'photo' in event['message']:
            send_message(event['message']['chat']['id'], f'Я не знаю що на це відповісти')

        elif 'sticker' in event['message']:
            send_message(event['message']['chat']['id'], 'Стікер. Навіщо?')

        elif 'text' not in event['message']:
            sticker = 'CAACAgIAAxkBAALpYWA8-lKTc_uJD0t6kstXWZGUjKgWAAK8AAP3AsgPM6_Lz2J4moEeBA'
            send_message(event['message']['chat']['id'], 'Я не знаю що це!')
            send_sticker(event['message']['chat']['id'], sticker)

        else:
            chat_id = event['message']['from']['id']
            word = event['message']['text']

            if word.startswith('/'):
                #або (if event['message']['text'][0] == '/')
                words = word.split()
                command = words[0][1:]
                name = event['message']['from']['first_name']

                if command == 'start':
                    sticker = 'CAACAgIAAxkBAALya2BHkDqOvyf0b0rgp6cRCEiknaxmAAK4AAP3AsgPRguctOJOCzgeBA'
                    send_sticker(chat_id, sticker)
                    key = [['/fact'],
                            ['/keys', '/help']]
                    keys = {'keyboard': key}
                    keyboard(chat_id, f'Вітаю, {name}!\nЯ тестовий бот\n'
                                      f'Можеш також щось спитати.\n через команду /what і слово,\n'
                                      f'наприклад: /what Сонце\n'
                                      f'/help\n'  
                                      f'/fact\n'
                                      f'/keys\n', keys)

                elif command == 'what':
                    what = word[6:]
                    urls = url_wiki(what)
                    inlinekeyboardbutton = [[{'text': 'Більше...'.title(), 'url': urls}]]
                    keys = {'inline_keyboard': inlinekeyboardbutton}
                    try:
                        summary = search_wiki(what)
                        photo = wiki_image(what)
                        send_photo_buttons(chat_id, photo, f'{summary} ...', keys)
                    except:
                        keyboard(chat_id, 'Не знаю. Можеш тут пошукати', keys)

                elif command == 'keys':
                    key = [['/start'],
                                 ['/fact', '/help']]
                    keys = {'keyboard': key}
                    keyboard(chat_id, '', keys)

                elif command == 'help':
                    sticker = 'CAACAgIAAxkBAALyg2BHlbGpWAsoSkWJ32UCe152Vg0fAAK5AAP3AsgPkCGq-DI3RtgeBA'
                    help_text = 'Тут написано які команди я знаю. В мене можна спитати якісь прості речі через команду /what і слово,\n' \
                                'наприклад: /what Сонце'
                    send_message(chat_id, help_text)
                    send_sticker(chat_id, sticker)

                elif command == 'fact':
                    facts = [
                        'Не достало бити по кнопках? Я більше нічого не знаю!!!',
                        'Вода мокра',
                        'Рогівка ока – єдина ділянка людського тіла, позбавлена кровоносної системи',
                        'Найбільшим в світі музеєм є Американський музей природної історії',
                        'Пісок колиться',
                    ]
                    text = random.choice(facts)
                    send_message(event['message']['chat']['id'], text)
                    if text == facts[0]:
                        sticker = 'CAACAgIAAxkBAALyfWBHk0IKGODpv2T8b3UR3MuEiMWrAALBAAP3AsgPYmYMc0ljJzkeBA'
                        send_sticker(chat_id, sticker)
                        inlinekeyboardbutton = [
                            [{'text': 'Start', 'url': 'https://core.telegram.org/bots/api#inlinekeyboardmarkup'}]]

                        reply_mark = {'inline_keyboard': inlinekeyboardbutton}
                        keyboard(chat_id, reply_mark)


                elif command == 'givemyid':
                    id_text = event['message']['from']['id']
                    send_message(chat_id, id_text)

                else:
                    sticker = 'CAACAgIAAxkBAALpZGA9Av35UJ6gxPa9n6Rrz2bT-t2wAALOAAP3AsgPXJhH4MyrbooeBA'
                    else_message = 'Я не знаю такої команди'
                    send_message(chat_id, else_message)
                    send_sticker(chat_id, sticker)

            else:
                send_message(event['message']['from']['id'], 'Це не схоже на команду')
                send_sticker(event['message']['from']['id'])

    return jsonify(event)


url_wiki_ua = 'https://uk.wikipedia.org/api/rest_v1/page/summary/'
url_wiki_en = 'https://en.wikipedia.org/api/rest_v1/page/summary/'


#дістає початок статті Вікі
def search_wiki(text):
    try:
        r = requests.get(url_wiki_ua + f"{text}")
        page = r.json()
        summary = page['extract'][0:512]
    except:
        r = requests.get(url_wiki_en + f"{text}")
        page = r.json()
        summary = page['extract'][0:512]
    return str(summary)

#дістає зображення для статті в Вікі


def wiki_image(text):
    try:
        r = requests.get(url_wiki_ua + f"{text}")
        page = r.json()
        image = page['originalimage']['source']
    except:
        r = requests.get(url_wiki_en + f"{text}")
        page = r.json()
        image = page['originalimage']['source']
    return str(image)

#URL статті


def url_wiki(text):
    try:
        r = requests.get(url_wiki_ua + f"{text}")
        page = r.json()
        urls = page["content_urls"]["desktop"]["page"]
    except:
        try:
            r = requests.get(url_wiki_en + f"{text}")
            page = r.json()
            urls = page["content_urls"]["desktop"]["page"]
        except:
            urls = 'https://en.wikipedia.org'
    return str(urls)


#Службова функція записує response в json

#def write_json(data, filename='answer.json'):
#    with open(filename, 'w', encoding='utf-8') as f:
#        json.dump(data, f, indent=2, ensure_ascii=False)


def keyboard(chat_id, text='keyboard', reply_markup=[]):
    url = URL + 'sendMessage'
    param = {'chat_id': chat_id,
             'text': text,
             'reply_markup': reply_markup}
    r = requests.post(url, json=param)
    return r.json()


def send_message(chat_id, text='s'):
    url = URL + 'sendMessage'
    answer = {'chat_id': chat_id, 'text': text}
    r = requests.post(url, json=answer)
    return r.json()


def send_sticker(chat_id, sticker="CAACAgIAAxkBAALpZGA9Av35UJ6gxPa9n6Rrz2bT-t2wAALOAAP3AsgPXJhH4MyrbooeBA"):
    url = URL + 'sendSticker'
    answer = {'chat_id': chat_id, 'sticker': sticker}
    r = requests.post(url, json=answer)
    return r.json()


def send_photo(chat_id, photo, caption):
    url = URL + 'sendPhoto'
    answer = {'chat_id': chat_id, 'photo': photo,
              'caption': caption}
    r = requests.post(url, json=answer)
    return r.json()


def send_photo_buttons(chat_id, photo, caption, reply_markup):
    url = URL + 'sendPhoto'
    answer = {'chat_id': chat_id, 'photo': photo,
              'caption': caption, 'reply_markup': reply_markup}
    r = requests.post(url, json=answer)
    return r.json()


if __name__ == '__main__':
    app.run()
